---
layout: markdown_page
title: "Category Direction - Helm Chart Registry"
---

- TOC
{:toc}

## Helm Chart Registry

Users or organizations that deploy complex pieces of software towards Kubernetes managed environments depend on a standardized way to automate provisioning those external environments. Helm is the package manager for Kubernetes and helps users define, manage, install, upgrade, and rollback even the most complex Kubernetes application. Helm uses a package format called Charts to describe a set of Kubernetes resources. There is a clear path towards utilizing GitLab's Container Registry and [new features in Helm 3](#whats-next--why) to provide a single location for Helm Charts within the Container Registry. GitLab will join the [open source community](https://github.com/helm/community) in enabling this capability and improve upon it with features targeted at our EE offering.

Helm charts will be easy to create, version, share and publish right within GitLab. This would provide an official and integrated method to publish, control, and version control Helm charts. 

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AHelm+Chart+Registry)
- [Overall Vision](/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

### Usecases listed

1. Public and private repositories for Helm charts
1. Fine-grained access control
1. Standardized workflow to version control and publish charts making use of GitLab's other services

## What's Next & Why

With the launch of Helm 3, which is now in [beta](https://helm.sh/blog/helm-3-preview-pt3/), pushing and pulling charts can now be done via OCI Registry. This means that users can now utilize the GitLab Container Registry for hosting Helm charts. However, due to the way metadata is passed and stored via Docker, it is not currently possible for us to parse this data and meet our performance standards. 

The first step is [gitlab-#207147](https://gitlab.com/gitlab-org/gitlab/-/issues/207147), which defines a new database schema for storing Docker manifests.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Distinguish between Helm Charts and Docker images in the GitLab user interface](https://gitlab.com/gitlab-org/gitlab/issues/38047)

## Competitive Landscape

* [Helm Hub](https://hub.helm.sh/)
* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Helm+Chart+Repositories)
* [Chart museum](https://chartmuseum.com/)
* [Codefresh](https://codefresh.io/features/#Helm)
* [Azure DevOps](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-helm-repos)

Helm Hub is the official Helm charts repository, which is supported by products like Artifactory from Jfrog and by Codefresh. Additionally, Chart museum offers an open sourced self-managed solution, aside from being able to code one yourself with GitLab pages, [Apache](https://medium.com/@maanadev/how-set-up-a-helm-chart-repository-using-apache-web-server-670ffe0e63c7), or by using a GH repo's [raw publishing url](https://hackernoon.com/using-a-private-github-repo-as-helm-chart-repo-https-access-95629b2af27c).

The Azure container registry can be used as a host for Helm chart repositories. With Helm 3 changing the storage backend to container registries, we are evaluating if we can offer the same level of support. 

## Top Customer Success/Sales Issue(s)

There are currently no customer success or sales issues. 

## Top Customer Issue(s)

[gitlab-#38047](https://gitlab.com/gitlab-org/gitlab/issues/38047) is currently the top customer issue. 

## Top Internal Customer Issue(s)

There are currently no internal customer issues. 

## Top Vision Item(s)

Our current vision is focused on implementing the MVC of the Helm Charts Repository as captured in [gitlab-#2281](https://gitlab.com/groups/gitlab-org/-/epics/2281).
