---
layout: handbook-page-toc
title: "CXC"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [CXC](https://cxcglobal.com/) and apply to team members who are contracted through CXC. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at CXC.

## New Zealand

* **Medical**
    * GitLab does not plan at this time to offer Private Health Insurance benefits because New Zealand residents can access free or subsidised medical care in New Zealand through the public healthcare system. Injuries and accidents are covered by the Accident Compensation Corporation cover.
* **Pension**
    * The base salary is inclusive of superannuation (known as wage bargaining in [KiwiSaver](https://www.kiwisaver.govt.nz/already/contributions/employers/)).
* **Life Insurance**
    * GitLab does not plan at this time to offer Life Insurance benefits because New Zealanders can access [government payments and services](https://www.workandincome.govt.nz/providers/health-and-disability-practitioners/health-and-disability-related-benefits.html) to help if they get ill, injured or have a disability.
