---
layout: handbook-page-toc
title: Working With Tickets
category: Handling tickets
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page provides a descriptive overview of how GitLab Support Engineers work the various views assigned to them in Zendesk in order to service our GitLab.com and Self-Managed customers and end-users within their entitled SLAs.

We strive to provide responses to our tickets within our established SLA. 
The SLAs described in [GitLab Support Service Levels](/support/#gitlab-support-service-levels) are based on ticket priority which can be set manually by support engineers. See [setting ticket priority](/handbook/support/workflows/setting_ticket_priority.html)

For an overview of the support we provide to our customers, please see the general [Support Page](/support/).

### Ticket Status

Each ticket in Zendesk has a [status](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets) that tells you what state it's currently in. They are as follows.

|  **Status** | **Meaning** | **Notes** |
| --- | --- | --- |
|  New | The ticket has just been opened and has had no replies. |  |
|  Open | The ticket has had one or more replies, and the customer is waiting on GitLab Support to provide the next reply. |  |
|  Pending | The ticket has been replied to and is waiting on the customer to provide additional information. | After 3 days in "Pending", a customer will receive a further response reminding them that the ticket exists. If there are no responses after a total of 7 days, the ticket will be moved to Solved. |
|  On-Hold | GitLab support is working on the ticket and may be waiting for information from another team | Placing a ticket on-hold will assign it to the engineer. After 3 days the ticket will move back to open status, requiring an update to the customer. On-hold is transparent to the customer (they see the status as 'Open') so there is no need to inform the customer that the ticket is being put on-hold. It's the engineer's responsibility to ensure timely replies or set the ticket back to 'Open' if they are no longer working on it. Setting a ticket to 'on-hold' while working on the ticket can be useful as it takes it out of the main queue, thus saving other engineers from wasting time reading the ticket. |
|  Solved | The ticket has been solved | A customer who replies to a Solved ticket will re-open it. A Solved ticket will transition to 'Closed' after 4 days. |
|  Closed | The ticket is archived | A customer who replies to a Closed ticket will open a new ticket with a note that relates the new case to the closed ticket. |

### Handling Large Files

Zendesk has a [maximum attachment size](https://support.zendesk.com/hc/en-us/articles/235860287-What-is-the-maximum-attachment-size-I-can-include-in-ticket-comments-) of 20MB _per file which can't be increased tany further. If you need a customer to share a larger file than this, then see [Provide Large Files to GitLab Support](/support/providing-large-files.html) for more info on how.

### Filling Out Ticket Fields

Depending on the queue you are working on and the form the ticket belongs to, you might need to fill out some ticket fields manually. Those fields help us capture important data that will help us improve the customer experience. As a high percentage of our tickets are solved/closed automatically through our workflows, it is important to make sure that before you submit your response to a ticket, you check that all required (*) fields and relevant non-required fields have been filled out.

### Copy contents of Slack threads to internal notes

When using Slack to work with others or communicate internally regarding a support ticket, please bear in mind [our chat retention policy](https://about.gitlab.com/handbook/communication/#slack) and the [Communication Guidelines (esp. 9.)](https://about.gitlab.com/handbook/communication/#general-guidelines). It's best for future searches in Zendesk to copy relevant advice, notes, ideas, etc. from Slack to an internal note in Zendesk.

## SLA Workflow

Our SLA workflow relies on end-users who submit tickets belonging to an organization and that organization having a GitLab Plan. Organization information is automatically added to Zendesk via a Salesforce Integration. We sync all records with Account Type equal to `Customer` from Salesforce to Zendesk. The information we get from Salesforce includes but it is not limited to: Account Owner, Technical Account Manager, GitLab Plan and Salesforce ID. Organizations should never be created manually in Zendesk as that can cause our sync to be ineffective.  If you think an Account in Salesforce doesn't have an equivalent Organization in Zendesk, please let the Support Operations Specialist know so a manual sync can be run.

### GitLab.com Views

Support Engineers with an Application focus should work on tickets in the following views in order:

1. GitLab.com Subscribers
1. GitLab.com Trials & Prospects
1. GitLab.com Free

Within a view, tickets should be prioritized based on the SLA time until breach.

### Self-Managed Views

Tickets should be picked up in the following order to make sure that SLAs are not breached (maximum SLA wait time exceeded) or (if breached) are addressed as soon as possible, and customers receive the proper level of service:

1. About to breach Premium tickets (breaches within 2 hours)
1. About to breach Starter tickets (breaches within 2 hours)
1. Breached Premium tickets
1. Breached Starter tickets
1. High priority Premium tickets
1. High priority Starter tickets
1. Medium priority Premium tickets
1. Medium priority Starter tickets
1. Low priority Premium tickets
1. Low priority Starter tickets

This would not apply to those working specialized roles, such as [FRT Hawk](support-modes-of-work.html#first-response-time-hawk) or [SLA Hawk](support-modes-of-work.html#sla-hawk).

#### Non-Support Views

1. Security
1. Upgrades, Renewals & AR (refunds)

### Hot Queue: How GitLab Manages Ticket Assignments

Traditional Support teams often use an "assignment" model, where an agent is assigned a ticket and guides the ticket through to completion. If they are stuck, they can re-assign this ticket to another agent to pick up and try and complete. At GitLab though, we take advantage of our global support team and use a system dubbed "Hot Queuing". This system means that we all work from one global queue and it's expected that multiple engineers will work together on most issues. This allows us to do the following:

- Keep tickets moving forward 24/7.
- Expose team members to more issues, so more learning is happening.
- Provide faster responses to customers, since tickets are not hidden away in an engineer's personal queue and "bound" to their workday.
- Everyone has an accurate overview of the support load.

#### When Should Tickets be Assigned?

There are times in the support process that an engineer may need to assign a ticket to themselves:

- If you are going to take the ticket to a call/screenshare, take it out of the queue by assigning the ticket to yourself. After the call, if the ticket is still on-going, feel free to unassign yourself so that everyone can contribute to moving it forward.
- If you've established an understanding with the customer that **you** will see the ticket through. There are times where we want this, and engineers have full discretion to do it, but these cases should be few and far between. Be aware of SLAs if you do this!

#### What if I Can't Answer a Ticket?

If you see a ticket in the queue that you are not able to answer, you should:

- Make an internal note with your initial gut feelings as to where the issue might be.
- CC yourself so you can follow along as the ticket progresses.
- If you know that other engineers looking at the ticket will need logs or version information, feel free to start the ball rolling with a simple response asking for it.
- Consider asking about the ticket in Slack to see if others can help.
- Consider asking a more experienced colleague, or take a look at the [Team page](/company/team/) to find someone who can help you.

With the hot queue, we all work together and no one should be scared to start a ticket.

#### What if Someone is Working on a Ticket that I’d Like to Work On?

If another engineer is looking at a ticket that you’re interested in working on, keep in mind:

- If an engineer is looking at a ticket, that doesn’t always indicate that they are actively working on it.
- If an engineer has a blue circle around their picture, it usually indicates that they are replying on the ticket. However, this can be a false positive.

If another engineer is looking at a ticket that you would like to work on:

- Contact that engineer via Slack to see if they are actively working on a ticket.
- If they are, ask to pair on the ticket, or offer any information and questions that you have.
- If they don’t respond, go ahead and work on the ticket (bias for action).

If you are working on a ticket, and need some time to research, indicate your involvement by:

- Leaving a note with your action plan and by what time you will respond.
- Be mindful of the remaining time in the SLA. If you aren't feeling confident that you can deliver within the SLA time, reach out to another engineer to pair on it.

### Zendesk SLAs and Breach Alerts

SLAs are set as Business Rules within Zendesk. For more information, please refer to the specific [Zendesk](/handbook/support/workflows/zendesk_admin.html) page.

We have a Slack integration that will notify us for self-managed, if a Premium or Ultimate ticket will breach within an hour, and for GitLab.com, if a Silver or Gold ticket will breach within 2 hours. If you see one of these, start a thread and consider this a _small emergency_. If you need help, draw the attention of other support engineers by tagging them, and work to move the ticket forward.

If a customer's reply is the last one in the ticket, do not set it to any status silently (except for Solved), because the breach clock will continue to run and the ticket may breach silently. Instead, send a confirmation, greeting, or other message, while also changing the status.

### Using On-Hold Status

You should use the On-hold status when it is necessary to do some internal work, e.g. reproduce a complex bug, discuss something with developers or wait for a session scheduled with a customer. When setting the status to On-hold it will be automatically assigned to you by the trigger [`Automatically assign on-hold ticket to an agent who put it to the on-hold status`](https://gitlab.zendesk.com/agent/admin/triggers/360033242313).

If you think that it should be assigned to someone else (e.g. session is scheduled for another engineer), feel free to re-assign it. Tickets without assignee will be automatically reopened by the trigger
[`Automatically reopen on-hold tickets without assignee`](https://gitlab.zendesk.com/agent/admin/triggers/360028981853). Tickets in on-hold status _with_ an assignee will be automatically reopened in 4 days by the automation [`Reopen on-hold tickets after 4 days`](https://gitlab.zendesk.com/agent/admin/automations/360028978393).

If a customer's reply is the last one in the ticket, do not set it to the On-hold status silently due to the same reasons as stated above in the
[Zendesk SLAs and Breach Alerts](#zendesk-slas-and-breach-alerts) section. Instead, reply to the ticket while also changing the status.

### Merging Tickets

If you're merging two customer tickets that are related and it's not 100% obvious to the customer, be sure to send a message letting them know why you're merging them. If you don't, it often causes confusion and they open follow ups asking why it was closed without comment.

Additionally, when [Merging Tickets](https://support.zendesk.com/hc/en-us/articles/203690916-Merging-tickets), leave `Requester can see this comment` unchecked in the ticket that's being merged into (the second ticket from the top) in order to maintain the SLA. If the merge comment is made public, Zendesk will consider it a response and will remove the SLA.

Also consider any ticket merge is final and there is no option to undo it.

### Removing Information From Tickets

We ask customers to send us logs and other files that are crucial in helping us solve the problems they are experiencing. If a customer requests deletion of information shared in a support ticket, if we suspect sensitive information was accidentally shared, ask a Support manager to delete the files using the [`Ticket Redaction`](https://www.zendesk.com/apps/support/ticket-redaction/) app. Only Zendesk administrators have access to this app.

To delete text or attachments from a ticket:

1. Go to the ticket in question and on the right hand nav bar, scroll down until you are able to locate the Ticket Redaction app.
1. In the text box, enter a string of text or source image URL you wish to redact.
1. If you wish to remove an attachment, you can click on the `Redact Attachment` button and choose the attachment you would like to remove.
